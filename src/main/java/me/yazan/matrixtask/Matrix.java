package me.yazan.matrixtask;

public class Matrix {

    private int rows;
    private int cols;
    private int matrix[][];

    public Matrix(int rows, int cols) throws IllegalArgumentException
    {

        if(rows < 1 || cols < 1)
            throw new IllegalArgumentException("Matrix cannot have a size lower than 1x1");

        this.matrix = new int[rows][cols];
        this.rows = rows;
        this.cols = cols;
    }

    public Matrix(int matrix[][]) throws IllegalArgumentException, NullPointerException
    {
        if (matrix == null)
            throw new NullPointerException("Matrix passed cannot be null");

        if (!isValid(matrix)) {
            throw new IllegalArgumentException("Matrix rows must have equal size");
        }
        this.rows = matrix.length;
        this.cols = matrix[0].length;
        this.matrix = new int[rows][cols];

        //?
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                this.matrix[i][j] = matrix[i][j];
    }


    private boolean isValid(int[][] matrix)
    {
        for (int i = 0; i < rows - 1; i++) {
            if (matrix[i] == null || matrix[i + 1] == null)
                return false;
            if (matrix[i].length != matrix[i + 1].length)
                return false;
        }
        return true;
    }

    public Matrix add(Matrix m) throws IllegalArgumentException
    {
        // TODO return a new instance, don't modify the current [SOLVED]

        Matrix res = new Matrix(m.rows,m.cols);

        if (this.rows != m.rows || this.cols != m.cols)
            throw new IllegalArgumentException("Matrix size mismatch");

        for (int i = 0; i < this.rows(); i++)
            for (int j = 0; j < this.columns(); j++)
                 res.matrix[i][j] = this.matrix[i][j] + m.matrix[i][j];

        return res;
    }

    public Matrix multiply(int k)
    {
        Matrix res = new Matrix(this.rows, this.cols);

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                res.matrix[i][j] = this.matrix[i][j] * k;
        return res;
    }

    private int[] row(int index)
    {
        int arr[] = new int[cols];
        for (int i = 0; i < cols; i++)
            arr[i] = matrix[index][i];
        return arr;
    }

    private int[] column(int index)
    {
        int arr[] = new int[rows];
        for (int i = 0; i < rows; i++)
            arr[i] = matrix[i][index];
        return arr;
    }

    private int dotProduct(int a[], int b[])
    {
        int dp = 0;
        for (int i = 0; i < a.length; i++)
            dp += a[i] * b[i];
        return dp;
    }

    public Matrix multiply(Matrix m) throws IllegalArgumentException
    {
        if (cols != m.rows)
            throw new IllegalArgumentException("Matrix's columns must equal the parameter matrix's rows");

        Matrix res = new Matrix(this.rows, m.cols);

        for (int i = 0; i < res.cols; i++)
            for (int j = 0; j < res.rows; j++)
                res.matrix[i][j] = dotProduct(row(i), m.column(j));
        return res;
    }

    public Matrix transpose()
    {
        Matrix transposed = new Matrix(rows, cols);

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                transposed.matrix[i][j] = matrix[j][i];

        return transposed;
    }

    public Matrix subMatrix(int row, int col)
    {
        if (row < 0 || row >= rows || col < 0 || col >= cols)
            throw new IndexOutOfBoundsException("Matrix index out of bounds");

        Matrix res = new Matrix(rows - 1, cols - 1);

        int k = 0;
        for (int i = 0; i < rows; i++) {
            if (i == row)
                continue;
            int x = 0;
            for (int j = 0; j < cols; j++) {
                if (j == col)
                    continue;
                res.matrix[k][x++] = matrix[i][j];
            }
            k++;
        }
        return res;
    }

    public Matrix squareMatrix(SquareType squareType) throws IllegalArgumentException
    {
        // TODO use enum [SOLVED]

        if (rows != cols)
            throw new IllegalArgumentException("Matrix must be square");

        Matrix res = new Matrix(rows, rows);

        if (squareType.equals(SquareType.DIAGONAL))
            copyDiagonal(res);
        else if (squareType.equals(SquareType.LOWER_TRIANGLE))
            copyLowerTriangle(res);
        else if (squareType.equals(SquareType.UPPER_TRIANGLE))
            copyUpperTriangle(res);
        else
            throw new NullPointerException("Square type passed is null");

        return res;
    }

    private void copyUpperTriangle(Matrix res)
    {
        for (int j = 0; j < cols; j++)
            for (int k = 0; k < rows - j; k++)
                res.matrix[k][j] = matrix[k][j];
    }

    private void copyLowerTriangle(Matrix res)
    {
        for (int j = cols - 1; j > 0; j--)
            for (int k = rows - j; k < rows; k--)
                res.matrix[k][j] = matrix[k][j];
    }

    private void copyDiagonal(Matrix res)
    {
        for (int i = 0; i < rows; i++)
            res.matrix[i][i] = matrix[i][i];
    }

    public int determinant()
    {
        int det = 0;

        if (rows == 2 && cols == 2)
            return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];

        for (int i = 0; i < cols; i++) {
            det += matrix[0][i] * subMatrix(0, i).determinant();
            det *= -1;
        }
        return det;
    }

    public int rows()
    {
        return rows;
    }

    public int columns()
    {
        return cols;
    }

    public void print() {
        System.out.println("----------");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++)
                System.out.printf("%d ", matrix[i][j]);
            System.out.println();
        }
        System.out.println("----------");
    }
private enum SquareType
{
    DIAGONAL,
    UPPER_TRIANGLE,
    LOWER_TRIANGLE;
}

}
