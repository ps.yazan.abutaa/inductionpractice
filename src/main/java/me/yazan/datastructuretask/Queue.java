package me.yazan.datastructuretask;

public class Queue {

    //TODO EXTRA circular array implementation to get rid of shifting
    //TODO EXTRA apply strategy design pattern (like stack) to get rid of enums
    private int pointer;
    private int dataSize;
    private int data[];
    private QueueType queueType;

    public Queue() {
        this.dataSize = 8;
        this.data = new int[dataSize];
        this.pointer = 0;
        this.queueType = QueueType.DYNAMIC;
    }

    public Queue(int dataSize) throws IllegalArgumentException {
        if (dataSize <= 0)
            throw new IllegalArgumentException("Queue error: passed size is invalid");
        this.dataSize = dataSize;
        this.data = new int[dataSize];
        this.pointer = 0;
        this.queueType = QueueType.STATIC;
    }

    public void enque(int element) throws IllegalStateException {
        if (pointer + 1 == dataSize) {
            if (this.queueType.equals(QueueType.STATIC))
                throw new IllegalStateException("Enque error: queue is full");
            expandQueue();
        }

        data[pointer++] = element;
    }

    public void deque() {
        if (pointer <= 0)
            throw new IllegalStateException("Deque error: queue is empty");

        shiftQueue();
        pointer--;
    }

    private void shiftQueue() {
        for (int i = 0; i < dataSize - 1; i++)
            data[i] = data[i + 1];
    }

    public int peek() {
        return data[0];
    }

    private void expandQueue() {
        dataSize = dataSize * 2;
        int newData[] = new int[dataSize];

        for (int i = 0; i < data.length; i++)
            newData[i] = data[i];

        this.data = newData;
    }

    public int size() {
        return this.pointer;
    }

    private enum QueueType {STATIC, DYNAMIC;}
}
