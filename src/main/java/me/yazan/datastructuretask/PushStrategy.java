package me.yazan.datastructuretask;

public interface PushStrategy<T> {

    void push(T value);
}
