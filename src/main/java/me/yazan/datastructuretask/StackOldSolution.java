package me.yazan.datastructuretask;

public class StackOldSolution<T> {
    private int size;
    private T data[];
    private int ptr;
    private StackType stackType;

    public StackOldSolution(int size) {
        this.size = size;
        data = (T[]) new Object[size];
        ptr = 0;
        stackType = StackType.STATIC;
    }

    public StackOldSolution() {
        this.size = 8;
        data = (T[]) new Object[size];
        stackType = StackType.DYNAMIC;
    }

    public void push(T element) throws IllegalStateException {
        if (isFull()) {
            // TODO read about state/strategy design pattern [SOLVED]
            //Solution: applied strategy design pattern as a better fit for this case than state.
            if (isStatic())
                throw new IllegalStateException("Push failed: stack limit reached");
            expandDynamicStack();
        }
        data[ptr++] = element;
    }

    private boolean isDynamic() {
        return stackType.equals(StackType.DYNAMIC);
    }

    private boolean isStatic() {
        return stackType.equals(StackType.STATIC);
    }

    private boolean isFull() {
        return ptr + 1 == size;
    }

    private void expandDynamicStack() {
        T newData[] = (T[]) new Object[size * 2];

        for (int i = 0; i < data.length; i++)
            newData[i] = data[i];
        this.data = newData;
        this.size = size * 2;
    }

    public void pop() throws IllegalStateException {
        // TODO decrement the size or remove its use and only depend on ptr [SOLVED]
        if (size == 0)
            throw new IllegalStateException("Pop failed: stack is empty");
        data[ptr--] = null;
    }

    public int size() {
        return ptr;
    }

    public T peek() {
        if (ptr - 1 < 0)
            throw new IllegalStateException("Peek failed: stack is empty");
        return data[ptr - 1];
    }

    //For debugging purposes
    public void print() {
        for (int i = 0; i < data.length; i++)
            System.out.printf("%s ", data[i]);
        System.out.println();
    }

    private enum StackType {
        DYNAMIC, STATIC
    }
}
