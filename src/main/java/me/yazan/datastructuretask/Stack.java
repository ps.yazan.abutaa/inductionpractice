package me.yazan.datastructuretask;

public class Stack<T> {

    private T data[];
    private PushStrategy pushStrategy;
    private int pointer = -1;

    public Stack(int size) {
        if (size < 1)
            throw new IllegalArgumentException("Invalid stack size");
        data = (T[]) new Object[size];
        pushStrategy = new StaticPushStrategy();
    }

    public Stack() {
        data = (T[]) new Object[8];
        pushStrategy = new DynamicPushStrategy();
    }

    public void push(T value) {
        pushStrategy.push(value);
        insertValue(value);
    }

    public void pop() throws IllegalStateException {
        if (isEmpty())
            throw new IllegalStateException("Pop failed: stack is empty");
        removeValue();
    }

    public T peek() {
        if (isEmpty())
            throw new IllegalStateException("Peek failed: stack is empty");
        return data[pointer];
    }

    public void print() {
        for (int i = 0; i < data.length; i++)
            System.out.printf("%s ", data[i]);
        System.out.println();
    }

    public boolean isFull() {
        return (pointer == data.length - 1);
    }

    public boolean isEmpty() {
        return (pointer == -1);
    }

    public int size() {
        return (pointer + 1);
    }

    private void insertValue(T value) {
        pointer++;
        data[pointer] = value;
    }

    private void removeValue() {
        data[pointer] = null;
        pointer--;
    }


    private class StaticPushStrategy implements PushStrategy {
        @Override
        public void push(Object value) {
            if (isFull())
                throw new IllegalStateException("Push failed: stack limit reached");
        }
    }

    public class DynamicPushStrategy implements PushStrategy {
        @Override
        public void push(Object value) {
            if (isFull())
                expandStack();
        }

        private void expandStack() {
            T newData[] = (T[]) new Object[data.length * 2];

            for (int i = 0; i < data.length; i++)
                newData[i] = data[i];
            data = newData;
        }
    }

}