package me.yazan.passwordtask;

import java.util.Random;

public enum PasswordSet {
    NUMBERS(fill('0', '9'), 2),
    UPPERCASE_LETTERS(fill('A', 'Z'), 2),
    SYMBOLS(new char[]{'_', '$', '#', '%'}, 2),
    MORESYMBOLS(fill(':', '?'), 2);

    private int count;
    private int originalCount;
    private char[] characters;
    private Random random;

    PasswordSet(char[] characters, int count) {
        this.count = count;
        this.originalCount = count;
        this.characters = characters;
        this.random = new Random();
    }

    public int count() {
        return count;
    }

    public char select() {
        int randomIndex = random.nextInt(characters.length);
        count--;
        return characters[randomIndex];
    }

    private static char[] fill(char start, char end) {
        char[] chars = new char[end - start];
        for (int i = 0; i < chars.length; i++)
            chars[i] = (char) (i + start);
        return chars;
    }

    public static void reset() {
        for (PasswordSet ps : PasswordSet.values())
            ps.count = ps.originalCount;
    }

}
