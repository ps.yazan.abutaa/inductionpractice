package me.yazan.passwordtask;

import me.yazan.datastructuretask.Stack;

import java.util.ArrayList;
import java.util.Random;

//TODO fix this class to work with new approach [DONE]
public abstract class OldPasswordGenerator {

    private final static int LENGTH = 8;
    private final static char[] SYMBOLS = {'_', '$', '#', '%'};
    private final static char[] DIGITS;
    private final static char[] CHARS;
    private static ArrayList<Integer> passwordPositions;
    private static StringBuilder password;
    private static Stack stack;
    private static Random random;

    static {
        DIGITS = fill('0', '9');
        CHARS = fill('A', 'Z');
    }

    private static char[] fill(char start, char end) {
        char[] chars = new char[end - start];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (i + start);
        }
        return chars;
    }

    private static void initVariables() {
        random = new Random();
        password = new StringBuilder(LENGTH);
        stack = new Stack();

        passwordPositions = new ArrayList<Integer>();

        for (int i = 0; i < LENGTH; i++) {
            passwordPositions.add(i);
            password.insert(i, ' ');
        }
    }

    public static String generate() {
        initVariables();
        fillStack();
        for (int i = 0; i < LENGTH; i++) {
            int randomPosIdx = random.nextInt(passwordPositions.size());
            password.insert(passwordPositions.get(randomPosIdx), stack.peek());
            passwordPositions.remove(passwordPositions.remove(randomPosIdx));
            stack.pop();
        }

        return password.toString().replace(" ", "");
    }

    private static void fillStack() {
        for (int i = 0; i < LENGTH; i++) {
            if (i < 4)
                addRandomNumber();
            else if (i > 3 && i < 6)
                addRandomSymbol();
            else
                addRandomUppercaseLetter();
        }
    }

    private static void addRandomNumber() {
        stack.push(DIGITS[random.nextInt(9 - 0 + 1) + 0]);
    }

    private static void addRandomSymbol() {
        stack.push(SYMBOLS[random.nextInt(3 - 0 + 1) + 0]);
    }

    private static void addRandomUppercaseLetter() {
        stack.push(CHARS[random.nextInt(90 - 65 + 1)]);
    }


}
