package me.yazan.passwordtask;

import java.util.Random;

public class PasswordGenerator {

    private int passwordLength;
    private Random random;


    public PasswordGenerator() {
        determineLength();
        random = new Random();
    }

    private void determineLength() {
        int length = 0;
        for (PasswordSet set : PasswordSet.values())
            length += set.count();
        passwordLength = length;
    }

    public String generate() {
        //Creating a new enum?
        PasswordSet.reset();
        return generatePassword();
    }

    private String generatePassword() {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < passwordLength; i++) {
            int setIndex = random.nextInt(PasswordSet.values().length);

            if (PasswordSet.values()[setIndex].count() > 0)
                stringBuilder.append(PasswordSet.values()[setIndex].select());
            else i--;
        }

        return stringBuilder.toString();
    }
}
