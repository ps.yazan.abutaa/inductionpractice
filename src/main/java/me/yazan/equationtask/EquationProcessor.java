package me.yazan.equationtask;

import me.yazan.datastructuretask.Stack;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//TODO fix this class to work with adding new operators easily [rework after TDD]
public class EquationProcessor {

    private Stack operands;
    private Stack operators;
    private Stack bracketStack;
    private String eqnOperands[];
    private char eqnOperators[];
    private int eqnSize;

    /* Why this solution is not optimal:
    - Addition of new operators is not easy
    - Minus works but isn't handled naturally
    - Stack types are inconsistent: character, string and a lot of parsing is confusing
    - Weak re-usability
    */
    public EquationProcessor(String equation) {
        checkEquationValidity(equation);

        eqnOperands = equation.split("[-+*/^()]");
        eqnOperators = equation.replaceAll("[0-9|.]", "").toCharArray();

        operands = new Stack();
        operators = new Stack();

        eqnSize = eqnOperands.length;
    }

    private void checkEquationValidity(String equation) throws IllegalStateException, NullPointerException {
        if (equation == null)
            throw new NullPointerException("Equation cannot be null");

        //bug: doesn't work on -+ for example (2 different consecutive operators)
        //bug: - not included
        Pattern p = Pattern.compile("(([-+*/])\\2+)");
        Matcher m = p.matcher(equation);

        if (equation.matches("^[a-zA-Z]*$") || m.find())
            throw new IllegalStateException("Equation is invalid");
    }

    public double evaluate() {
        String evalAsString = "0.0";
        operands.push(eqnOperands[0]);
        for (int i = 1; i < eqnSize; i++) {
            operands.push(eqnOperands[i]);

            if (OperatorUtil.checkPrio((Character) operators.peek()) == 1)
                calculate();
            if ((Character) operators.peek() == '-')
                switchNegativeSign();

            if (i >= eqnSize - 1)
                break;

            operators.push(eqnOperators[i]);
        }

        evalAsString = sumOperandsAndOperations(evalAsString);

        String lastOperand = (String) operands.peek();
        evalAsString = OperatorUtil.performOperation('+', evalAsString, lastOperand);
        return Double.parseDouble(evalAsString);
    }

    /*
    Combine all available operands with their corresponding operations in their stacks
     */
    private String sumOperandsAndOperations(String evalAsString) {
        int preOpSize = operators.size();
        for (int i = 0; i < preOpSize; i++) {
            char operator = (Character) operators.peek();
            String num = (String) operands.peek();

            evalAsString = OperatorUtil.performOperation(operator, evalAsString, num);
            operators.pop();
            operands.pop();
        }
        return evalAsString;
    }


    /*
    Adds the sign of the operand to its self instead of the operations stack
     */
    private void switchNegativeSign() {
        operators.pop();
        operators.push('+');
        String signSwitchNum = "-" + operands.peek();
        operands.pop();
        operands.push(signSwitchNum);
    }

    /*
    Combines the last two operands in the stack by performing the operation between them
     */
    private void calculate() {
        String right = (String) operands.peek();
        operands.pop();
        String left = (String) operands.peek();
        operands.pop();

        String squashedValue = OperatorUtil.performOperation((Character) operators.peek(), left, right);

        operands.push("0");
        operands.push(squashedValue);
        operators.pop();
        operators.push('+');
    }
}

