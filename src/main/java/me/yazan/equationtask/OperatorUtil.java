package me.yazan.equationtask;


public final class OperatorUtil {


    public static int checkPrio(char c) {
        if (c == '^')
            return 2;
        if (c == '*' || c == '/')
            return 1;
        return 0;
    }

    public static String performOperation(char operation, String firstNum, String secondNum) {
        double result;

        switch (operation) {
            case '+':
                result = Double.parseDouble(firstNum) + Double.parseDouble(secondNum);
                break;
            case '-':
                result = Double.parseDouble(firstNum) - Double.parseDouble(secondNum);
                break;
            case '*':
                result = Double.parseDouble(firstNum) * Double.parseDouble(secondNum);
                break;
            case '/':
                result = Double.parseDouble(firstNum) / Double.parseDouble(secondNum);
                break;
            case '^':
                result = Math.pow(Double.parseDouble(firstNum), Double.parseDouble(secondNum));
                break;
            default:
                throw new IllegalArgumentException("Operator " + operation + " does not exist");
        }

        return Double.toString(result);
    }


}
