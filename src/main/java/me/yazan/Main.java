package me.yazan;

import me.yazan.binarytreetask.BST;
import me.yazan.equationtask.EquationProcessor;
import me.yazan.matrixtask.Matrix;
import me.yazan.passwordtask.PasswordGenerator;

import java.util.Scanner;

public class Main {

    public static void main(String args[]) {

        int[][] values = {{1, 3,1,4}, {3,9,5,15},{0,2,1,1},{0,4,2,3}};

        Matrix m1 = new Matrix(values);
        System.out.println(m1.determinant());
        m1.print();
        System.out.println(m1.determinant());

        System.out.println("task3--------");


        //String equation = "5+3**5";
        Scanner scanner = new Scanner(System.in);

        //String equation = "500+3-3*50/2*100";
        //System.out.println("Enter an equation: ");
        //String equation = scanner.nextLine();
        //EquationProcessor eq = new EquationProcessor(equation);
        // System.out.println("Equation Processor: " + eq.evaluate());


        System.out.println("task4--------");
        BST bst = new BST();

        bst.accept(10);
        bst.accept(9);
        bst.accept(12);
        bst.accept(15);

        System.out.println("Tree Depth: " + bst.treeDepth());
        System.out.println(bst.depth(12));
        System.out.println("task5--------");


        PasswordGenerator pg = new PasswordGenerator();

        System.out.println(pg.generate());
        System.out.println(pg.generate());
        System.out.println(pg.generate());
        System.out.println(pg.generate());
        System.out.println(pg.generate());

       // for(int i = 0; i < 2; i++)
       //     System.out.println("Generated: " + PasswordGenerator.generate());

    }
}