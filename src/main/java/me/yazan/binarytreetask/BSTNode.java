package me.yazan.binarytreetask;

import java.util.function.Consumer;

public class BSTNode {
    private BSTNode left;
    private BSTNode right;
    private int value;

    public BSTNode(int value) {
        this.value = value;
    }

    public BSTNode left() {
        return this.left;
    }

    public BSTNode right() {
        return this.right;
    }

    //TODO understand lambda expression used in below 2 methods
    public boolean accept(int value) {
        if (value == this.value)
            return false;
        if (value > this.value) {
            return accept(value, this.right, n -> right = n);
        }
        return accept(value, this.left, n -> this.left = n);
    }

    private boolean accept(int value, BSTNode node, Consumer<BSTNode> assigner) {
        if (node == null) {
            assigner.accept(new BSTNode(value));
            return true;
        }
        return node.accept(value);
    }

    private boolean acceptToLeft(int value) {
        if (this.left == null) {
            this.left = new BSTNode(value);
            return true;
        }
        return this.left.accept(value);
    }

    private boolean acceptToRight(int value) {
        if (this.right == null) {
            this.right = new BSTNode(value);
            return true;
        }
        return this.right.accept(value);
    }

    public int searchNode(int value) {

        if (value == this.value())
            return 1;

        if (isLeaf())
            return -1;

        if (value > this.value())// TODO I could have one branch left or right
            return this.right.searchNode(value) + 1;
        return this.left.searchNode(value) + 1;
    }

    private boolean isLeaf() {
        return this.left() == null && this.right() == null;
    }

    public int calculateDepth() {

        if (this.left == null || this.right == null)
            return 1;
        int left = this.left.calculateDepth();
        int right = this.right.calculateDepth();

        return Math.max(left, right) + 1;
    }

    public int value() {
        return this.value;
    }

}
