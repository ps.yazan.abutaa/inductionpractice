package me.yazan.binarytreetask;

public class BST {

    private BSTNode root;
    private TreeState treeState;

    public BST() {
        root = null;
        treeState = new EmptyTree();
    }

    //TODO: apply state design pattern to get rid of this redundant null check [SOLVED]
    public boolean accept(int value) {
        return treeState.onAccept(value);
    }

    private void setTreeState(TreeState treeState) {
        this.treeState = treeState;
    }

    public int treeDepth() {
        return this.treeState.onCalculateTreeDepth();
    }


    public int depth(int value) {
        return this.treeState.onCalculateDepth(value);
    }


    private abstract class TreeState {
        public abstract boolean onAccept(int value);
        public abstract int onCalculateTreeDepth();
        public abstract int onCalculateDepth(int value);
    }

    private class EmptyTree extends TreeState {

        @Override
        public boolean onAccept(int value) {
            root = new BSTNode(value);
            BST.this.setTreeState(new RootedTree());
            return true;
        }

        @Override
        public int onCalculateTreeDepth() throws IllegalStateException{
            throw new IllegalStateException("Cannot calculate tree depth when root is null");
        }

        @Override
        public int onCalculateDepth(int value) {
            throw new IllegalStateException("Cannot find depth when root is null");
        }
    }

    private class RootedTree extends TreeState {
        @Override
        public boolean onAccept(int value) {
            return root.accept(value);
        }

        @Override
        public int onCalculateTreeDepth() {
            return root.calculateDepth() + 1;
        }

        @Override
        public int onCalculateDepth(int value) {
            return root.searchNode(value);
        }
    }
}
